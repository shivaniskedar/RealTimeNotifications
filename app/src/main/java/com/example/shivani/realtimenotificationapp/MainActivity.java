package com.example.shivani.realtimenotificationapp;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;

import com.example.shivani.realtimenotificationapp.helper.DestinationLocationInfo;
import com.example.shivani.realtimenotificationapp.helper.GPSTracker;
import com.example.shivani.realtimenotificationapp.helper.NotificationService;
import com.example.shivani.realtimenotificationapp.helper.PlacesArrayAdapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,ActivityCompat.OnRequestPermissionsResultCallback {
    @BindView(R.id.txt_current_location) TextView txtCurrentLocation;
    @BindView(R.id.autoCompleteTextView) AutoCompleteTextView autoCompleteTextView;
    @BindView(R.id.btn_start_stop) Button btnStartStop;
    private GoogleApiClient mGoogleApiClient;
    private static final int GOOGLE_API_CLIENT_ID = 0;
    private PlacesArrayAdapter mPlaceArrayAdapter;
    private LatLngBounds BOUNDS_MOUNTAIN_VIEW;
    GPSTracker gpsTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        PermissionHandler.getSharedInstance().checkForPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        getCurrentLocation();

    }

    private void getCurrentLocation(){
        gpsTracker = new GPSTracker(this);
        if (isNetworkAvailable()) {

            if (gpsTracker.canGetLocation) {

                Geocoder geocoder = new Geocoder(this, Locale.getDefault());

                List<Address> addresses = null;
                try {
                    addresses = geocoder.getFromLocation(gpsTracker.latitude, gpsTracker.longitude, 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                txtCurrentLocation.setText(addresses.get(0).getAddressLine(0));
                searchPlace();
            } else gpsTracker.showSettingsAlert();
        }else showNetworkDIalog();
    }

    public void showNetworkDIalog(){
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(this);
        // Setting Dialog Message
        alertDialog.setMessage("You are offline. Turn on the network.");

        // On pressing Settings button
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                gpsTracker.showSettingsAlert();
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                gpsTracker.showSettingsAlert();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void searchPlace(){
        //For place search
        BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude()), new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude()));
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                .addConnectionCallbacks(this)
                .build();
        autoCompleteTextView.setThreshold(4);

        autoCompleteTextView.setOnItemClickListener(mAutocompleteClickListener);
        AutocompleteFilter filter = new AutocompleteFilter.Builder().build();
        mPlaceArrayAdapter = new PlacesArrayAdapter(this, android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, filter);
        autoCompleteTextView.setAdapter(mPlaceArrayAdapter);

        autoCompleteTextView.setDropDownBackgroundResource(R.color.colorWhite);
        autoCompleteTextView.setDropDownWidth(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlacesArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);

            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                places.getStatus().toString();
                return;
            }
        }
    };

    @OnClick(R.id.btn_start_stop) public void click(){
        DestinationLocationInfo destinationLocationInfo = DestinationLocationInfo.getSharedInstance();
        destinationLocationInfo.setDestLocLattitude("18.590773");
        destinationLocationInfo.setDestLocLongitude("73.771214");
        Intent intent = new Intent(MainActivity.this,NotificationService.class);
        startService(intent);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getCurrentLocation();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
