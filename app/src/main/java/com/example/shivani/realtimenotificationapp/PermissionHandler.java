package com.example.shivani.realtimenotificationapp;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;

public class PermissionHandler {
    public static final int PERMISSION_ACCESS_COARSE_AND_FINE_LOCATION = 104;

    private static PermissionHandler mPermissionHandler;

    public static void initialize() {
        mPermissionHandler = new PermissionHandler();
    }

    public static PermissionHandler getSharedInstance() {
        if (mPermissionHandler == null)
            mPermissionHandler = new PermissionHandler();
        return mPermissionHandler;
    }

    public static boolean isRunningMarshmallowOrAbove() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    public boolean checkForPermission(Context context, String permissionName) {
        if (isRunningMarshmallowOrAbove()) {
            return (ContextCompat.checkSelfPermission(context, permissionName) == PackageManager.PERMISSION_GRANTED);
        } else
            return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void askForPermissions(Activity context, String[] permissionList, int requestCode) {
        context.requestPermissions(permissionList, requestCode);
    }
}
