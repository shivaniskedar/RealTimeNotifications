package com.example.shivani.realtimenotificationapp.helper;

import android.content.Context;
import android.content.SharedPreferences;

public class DestinationLocationInfo {
    private static DestinationLocationInfo mInstance;
    private static SharedPreferences mPref;

    private static final String KEY_PREF_NAME = "LOC_INFO";
    private static final String KEY_DEST_LOC_LATTITUDE = "KEY_DEST_LOC_LATTITUDE";
    private static final String KEY_DEST_LOC_LONGITUDE = "KEY_DEST_LOC_LONGITUDE";

    private DestinationLocationInfo(Context context) {
        mPref = context.getSharedPreferences(KEY_PREF_NAME, Context.MODE_PRIVATE);
    }

    public static synchronized void initialize(Context context) {
        if (mInstance == null)
            mInstance = new DestinationLocationInfo(context);
    }

    public static synchronized DestinationLocationInfo getSharedInstance() {
        if (mInstance == null) {
            throw new IllegalStateException(DestinationLocationInfo.class.getSimpleName() + " is not initialized, call initialize(..) method first.");
        }
        return mInstance;
    }

    public void setDestLocLattitude(String value) {
        mPref.edit().putString(KEY_DEST_LOC_LATTITUDE, value).commit();
    }

    public String getDestLocLattitude() {
        return mPref.getString(KEY_DEST_LOC_LATTITUDE, null);
    }

    public void setDestLocLongitude(String value) {
        mPref.edit().putString(KEY_DEST_LOC_LONGITUDE, value).commit();
    }

    public String getDestLocLongitude() {
        return mPref.getString(KEY_DEST_LOC_LONGITUDE, null);
    }

}
