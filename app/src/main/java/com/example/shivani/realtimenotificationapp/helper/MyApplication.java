package com.example.shivani.realtimenotificationapp.helper;

import android.support.multidex.MultiDexApplication;

public class MyApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        DestinationLocationInfo.initialize(getApplicationContext());
    }
}
